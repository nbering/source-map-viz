#!/usr/bin/env node

const parseArgs = require("./lib/util/parse-args")

const options = parseArgs(process.argv);

require("./lib/main")(options).toConsole();
