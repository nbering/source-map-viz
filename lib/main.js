const fs = require("fs");
const os = require("os");
const colors = require("colors/safe");
const {SourceMapConsumer, SourceMapGenerator, SourceNode} = require("source-map");

function SourceMapViz(options)
{
    this.noLines = options.noLines;
    this.generatedFile = options.generatedFile;
    this.sourceMapUrl = options.sourceMapUrl;
    this._readSourceFile(options.generatedFile);
    this._readMapFile(this.sourceMapUrl);
    this._assignColorsToNodes();
    this.highlightedGeneratedCode =
        this._colorNodes(this.generatedCode, this.generatedFile, "generated");

    this.highlightedOriginalSource = {};
    this.map.sources.forEach((source, index) =>
    {
        this.highlightedOriginalSource[source] =
            this._colorNodes(this.map.sourcesContent[index], source, "original");
    });
};

SourceMapViz.prototype._readMapFile = function()
{
    if (!this.sourceMapUrl)
        throw "No source map file specified (may be missing source map url comment from file).";
    const mapString = fs.readFileSync(this.sourceMapUrl).toString();
    this.map = JSON.parse(mapString);
    this.sourceMapConsumer = new SourceMapConsumer(this.map);
    if (!this.sourceMapConsumer.hasContentsOfAllSources())
        throw "Non-inline sources not yet supported.";
    this.sourceMapConsumer.computeColumnSpans();
}

SourceMapViz.prototype._readSourceFile = function(filePath)
{
    const code = fs.readFileSync(this.generatedFile).toString();
    const sourceMapRegex = /\n\/[*/][@#]\s+sourceMappingURL=((?:(?!\s+\*\/).)*).*/;
    if (sourceMapRegex.test(code))
        this.sourceMapUrl = this.sourceMapUrl || RegExp.$1;
    this.generatedCode = code;
}

SourceMapViz.prototype._assignColorsToNodes = function()
{
    this.sourceNodes = [];
    this.sourceMapConsumer.eachMapping(mapping =>
    {
        this.sourceNodes.push({
            source: mapping.source,
            generatedLine: mapping.generatedLine,
            generatedColumn: mapping.generatedColumn,
            originalLine: mapping.originalLine,
            originalColumn: mapping.originalColumn,
            name: mapping.name,
            color: nextColor()
        });
    });
}

SourceMapViz.prototype._colorNodes = function(code, filename, originalOrGenerated)
{
    const lineVar = originalOrGenerated + "Line";
    const colVar = originalOrGenerated + "Column";
    const refinedMappings = this.sourceNodes
        .filter(m =>
        {
            return originalOrGenerated === "generated" || m.source === filename;
        })
        .sort((a, b) =>
        {
            if (a[lineVar] > b[lineVar] ||
                ( a[lineVar] === b[lineVar] && a[colVar] > b[colVar]))
                return 1;
            if (a[lineVar] < b[lineVar] ||
                ( a[lineVar] === b[lineVar] && a[colVar] < b[colVar]))
                return -1;

            return 0;
        });

    const lines = code.split(os.EOL);

    let lastRow = null, lastCol = null;
    let rowBuffer = "";
    for (let i = refinedMappings.length - 1; i >=-1; i--)
    {
        // Handle change in row.
        if (i < 0 || refinedMappings[i][lineVar] !== lastRow)
        {
            if (lastRow !== null)
            {
                // Not the first time around.

                // Tack the begining of the last line onto the row buffer
                rowBuffer = lines[lastRow - 1].substring(0,lastCol) + rowBuffer;

                // Replace to original line with the row buffer.
                lines[lastRow-1] = rowBuffer;
                rowBuffer = "";

                // This was the last time through, to flush the buffer.
                if (i < 0)
                    break;
            }
            lastCol = lines[refinedMappings[i][lineVar] - 1].length;
        }

        rowBuffer = refinedMappings[i].color(lines[refinedMappings[i][lineVar] - 1]
            .substring(refinedMappings[i][colVar], lastCol)) + rowBuffer;

        lastRow = refinedMappings[i][lineVar];
        lastCol = refinedMappings[i][colVar];
    }

    let output = "";
    if (this.noLines)
        output = lines.join("\n");
    else
        lines.forEach((line, index)=>
            {
                output += `${index + 1}: ${line}\n`;
            });

    return output;
}

SourceMapViz.prototype.toConsole = function()
{
    console.log(`=== ${this.generatedFile} ===`);
    console.log(this.highlightedGeneratedCode);

    for (const p in this.highlightedOriginalSource)
    {
        if (this.highlightedOriginalSource.hasOwnProperty(p))
        {
            console.log(`=== ${p} ====`);
            console.log(this.highlightedOriginalSource[p]);
        }
    }
}

const usedColors = [colors.white.bgBlack, colors.white.bgRed, colors.white.bgBlue, colors.white.bgMagenta, colors.white.bgCyan, colors.bgWhite];
var colorIndex = 0;

function nextColor()
{
    if (colorIndex >= usedColors.length)
        colorIndex = 0;

    return usedColors[colorIndex++];
}

module.exports = function(options)
{
    return new SourceMapViz(options);
}
