module.exports = function(argv){
    const result = {};
    const args = argv.slice(2);

    while(args.length)
    {
        const inputArg = args.shift();

        switch(inputArg)
        {
            case "--no-color":
                throw "No color? Nothing to see here. Move along.";
            case "--no-lines":
                result.noLines = true;
                break;
            case "--source-map":
            case "-m":
                result.sourceMapUrl = args.shift();
                break;
            default:
                result.generatedFile = inputArg;
        }
    }

    return result;
}
