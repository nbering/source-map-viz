# source-map-viz

A commandline tool for visualizing the nodes in a source map.

> **Work in Progress**: Does not support embedded maps. Requires inlined-sources.

### Install

```sh
# Login (Currently a private module.)
$ npm login

$ npm install -g @nbering/source-map-viz
```


### Usage

```sh
#Basic
$ source-map-viz generated-file.js

#Manually Set Source Map
$ source-map-viz --source-map generated-file.js.map generated-file.js

#No Line Numbers in Output
$ source-map-viz --no-lines generated-file.js
```
